package emilianwilczek;

public class IntegerSorter
{
    public static void displayArr(int[] integerArray)
    {
        for(int i = 0; i < integerArray.length; i++)
        {
            System.out.println(integerArray[i]);
        }
        System.out.println("");
    }

    public static void bubbleSort(int[] integerArray)
    {
        int arrayLength = integerArray.length;
        boolean swap = true;

        int j = 0;

        while (swap)
        {
            swap = false;
            j++;
            for (int i = 0; i < arrayLength - j; i++)
            {

                if (integerArray[i] > integerArray[i + 1])
                {
                    //maths swap
                    //integerArray[i] = integerArray[i] + integerArray[i + 1];
                    //integerArray[i + 1] = integerArray[i] - integerArray[i + 1];
                    //integerArray[i] = integerArray[i] - integerArray[i + 1];

                    //XOR swap
                    integerArray[i] = integerArray[i] ^ integerArray[i + 1];
                    integerArray[i + 1] = integerArray[i] ^ integerArray[i + 1];
                    integerArray[i] = integerArray[i] ^ integerArray[i + 1];
                    swap = true;
                }
            }
        }
    }

    public static void bubbleSortReverse(int[] integerArray)
    {
        int arrayLength = integerArray.length;
        boolean swap = true;

        int j = 0;

        while (swap)
        {
            swap = false;
            j++;
            for (int i = 0; i < arrayLength - j; i++)
            {

                if (integerArray[i] < integerArray[i + 1])
                {
                    //maths swap
                    //integerArray[i] = integerArray[i] + integerArray[i + 1];
                    //integerArray[i + 1] = integerArray[i] - integerArray[i + 1];
                    //integerArray[i] = integerArray[i] - integerArray[i + 1];

                    //XOR swap
                    integerArray[i] = integerArray[i] ^ integerArray[i + 1];
                    integerArray[i + 1] = integerArray[i] ^ integerArray[i + 1];
                    integerArray[i] = integerArray[i] ^ integerArray[i + 1];
                    swap = true;
                }
            }
        }
    }

    public static void main(String[] args)
    {
        int[] integerArray = {1, 4, 2, 6, 8, 3, 5, 7, 9, 10};

        displayArr(integerArray);

        //sort array
        bubbleSort(integerArray);
        displayArr(integerArray);

        //reverse sort array
        bubbleSortReverse(integerArray);
        displayArr(integerArray);
    }

}
